# Blogging Guide

* The length should be around 500 to max 1500 words
* Big size font ( has to be easy to read on screen )
* The structure : Question? - Problem Solution - How we help solve this
* Paragraphs are shorter: Because you have to scroll a lot when reading on a
screen, paragraphs tend to be shorter so a whole thought can fit in a browser
window.

* Important points are highlighted: Online readers tend to skim through pieces, so
let us put key points in bold type so their readers can easily pick out the crucial
stuff.

* Bulleted lists are common: Bulleted lists are another way that skimmers are
accommodated, making all the main points easily available.

* It contains links to other sites: links to our own blog (related articles) or webpage
* It’s conversational in tone: Blog writing tends to be a little more personal
than most writing. What readers tend to respond to is the writer’s unique
voice, their personality as expressed through their writing. That means
you can use “I” and “you”, you can use slang.

* Call to Action – How will you engage your readers to reach your ROI? Here are
some phrases to include in your blog:
    * Join my email list
    * Download my ebook
    * Buy my product
    * Sign up for my service
    * Call or email me for a consultation
    * Buy advertising on this blog
    * Connect with me on various social
      sites (Facebook, Twitter, LinkedIn,
      MySpace etc.)
    * Visit my store or office
    * Come meet me at this live event
* So before you begin writing, outline your blog post. This will make it much easier
for you down the road -- these bullet points essentially outline the story of your
blog post, and then all you have to do is fill in the blanks. Then, take those bullet
points and format them the way you'd like them to appear in your final product
-- add H2 HTML tags to make larger sections stand out, and bold and italicize
as need. Then, it's time to focus on the content of your blog post.


![The Perfect Blog Post](https://socialtriggers.com/wp-content/uploads/2012/06/perfectblogpost1.png)
![The Anatomy of Blog Post](https://neilpatel-qvjnwj7eutn3.netdna-ssl.com/wp-content/uploads/2015/07/ab.jpg)
